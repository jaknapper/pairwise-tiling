import matplotlib.pyplot as plt
import cv2
import numpy as np
import os
from collections import namedtuple
import matplotlib.cm as cm
from matplotlib.lines import Line2D

Rectangle = namedtuple('Rectangle', 'xmin ymin xmax ymax')

def import_images(dir, px_per_step):
    '''Takes a link to a dir, and returns a list of images
    Along with the array of each image, also contains the x,y,z stage position,
    and the estimated x,y position in pixels according to the calibration'''
    list = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir,f)) and f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif'))]
    list.sort(key=lambda x: os.path.getmtime(os.path.join(dir, x)))
    images = []

    for link in list:
        x_stage = float(link.split('_')[-3])
        y_stage = float(link.split('_')[-2])
        z_stage = float(link.split('_')[-1].split('.')[0])

        link = os.path.join(dir, link)
        img = cv2.imread(link, -1)

        # I know this is just a matrix multiplication, but the jumps from x in pixels to coords hurts my head
        x = (x_stage * px_per_step[0][1]) * (img.shape[1] / 832)
        y = (y_stage * px_per_step[1][0]) * (img.shape[0] / 624)
        x += (y_stage * px_per_step[1][1]) * (img.shape[0] / 624)
        y += (x_stage * px_per_step[0][0]) * (img.shape[1] / 832)

        images.append([img, x_stage, y_stage, z_stage, x, y])

    return images

def find_overlapping_pairs(images, thresh = 0.1):
    '''Takes a list of images, and makes a list of those that overlap by thresh * their area
    Also returns an estimate of the offset between them in pixels'''
    height, width = images[0][0].shape[:-1]

    pairs = []
    for i in range(len(images)):
        centre_x = images[i][1]
        centre_y = images[i][2]
        
        # A rectangle in pixels coords corresponding to the image position
        p_centre_x = images[i][4]
        p_centre_y = images[i][5]
        left = p_centre_x - width/2
        right = p_centre_x + width/2
        top = p_centre_y + height/2
        bottom = p_centre_y - height/2
        ra = Rectangle(left, bottom, right, top)

        # Goes through every remaining image possible pair, checks if they overlap

        for j in range(i+1, len(images)):
            centre_1x = images[j][1]
            centre_1y = images[j][2]
            
            p_centre_1x = images[j][4]
            p_centre_1y = images[j][5]
            left_1 = p_centre_1x - width/2
            right_1 = p_centre_1x + width/2
            top_1 = p_centre_1y + height/2
            bottom_1 = p_centre_1y - height/2
            rb = Rectangle(left_1, bottom_1, right_1, top_1)
            
            if area(ra, rb) > thresh * height * width:
                if abs(left - left_1) < width * 0.2 or abs(top - top_1) < height * 0.2: # extra check to make sure they don't have a thin overlap in one axis that wouldn't work as well
                    pairs.append((i, j, [left_1 - left, top_1 - top], [centre_1x - centre_x, centre_1y - centre_y]))
    return pairs

def area(a, b):  # returns 0 if rectangles don't intersect
    dx = min(a.xmax, b.xmax) - max(a.xmin, b.xmin)
    dy = min(a.ymax, b.ymax) - max(a.ymin, b.ymin)
    if (dx>=0) and (dy>=0):
        return dx*dy
    else:
        return 0

def cross_corr_pair(pairs, images, method, buffer = 0.1):
    '''Take pairs of images we know overlap
    Estimate the offset between them using a least squares correlation'''
    height, width = images[0][0].shape[:-1]
    shifts = []
    method = eval(method)
    rel_confs = []
    buffer = buffer / 2

    # Shape of pair is '[image 1 index, image 2 index, [estimated pixel offset from stage]]'
    for pair in pairs:
        img_1 = images[pair[0]]
        img_2 = images[pair[1]]        
        
        # estimate the location of their overlap based on the stage coordinates
        if pair[2][0] >= 0:
            start_x = int(pair[2][0])
            end_x = int(width)
        else:
            start_x = 0
            end_x = int(pair[2][0])

        if pair[2][1] >= 0:
            start_y = int(pair[2][1])
            end_y = int(height)
        else:
            start_y = 0
            end_y = int(pair[2][1])
        
        overlapped = img_1[0]
        overlapped = overlapped[start_y:end_y, start_x:end_x]
        overlap_height, overlap_width = overlapped.shape[:-1]
        
        # we don't want to go right up to the edge in case the stage didn't move exactly right
        # make the estimated overlapping region smaller by buffer / 2 in each axis
        overlapped = overlapped[int(buffer * overlap_height): int(-buffer * overlap_height), int(buffer * overlap_width) : int(-buffer * overlap_width)]
        overlap_top_left = (start_x + int(buffer * overlap_width),start_y + int(buffer * overlap_height))

        overlapped_grey = cv2.cvtColor(overlapped, cv2.COLOR_BGR2GRAY)
        img_2_grey = cv2.cvtColor(img_2[0], cv2.COLOR_BGR2GRAY)
        corr = cv2.matchTemplate(img_2_grey, overlapped_grey, method)

        mn,mx,mnLoc,mxLoc = cv2.minMaxLoc(corr)

        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = mnLoc
            conf = mn
        else:
            top_left = mxLoc
            conf = mx
        
        MPx = top_left[0]
        MPy = top_left[1]
        
        shift = (overlap_top_left[0] - MPx, overlap_top_left[1] - MPy)
        shifts.append(shift)

        MPx = shift[0]
        MPy = shift[1]
    
        rel_conf = conf / np.mean(corr)
        rel_confs.append(rel_conf)
    
    plt.plot(rel_confs,'.')
    return pairs, shifts, rel_confs

def plot_displacements(pairs, shifts, rel_confs):
    fig, ax = plt.subplots()
    colors = cm.rainbow(np.linspace(0, 1, len(pairs)))
    for i in range(len(pairs)):
        ax.scatter(pairs[i][2][0],pairs[i][2][1], color = colors[i], marker= '.')
        ax.scatter(shifts[i][0], shifts[i][1], color = colors[i], marker='x')
        ax.arrow(pairs[i][2][0],pairs[i][2][1], shifts[i][0]-pairs[i][2][0], shifts[i][1]-pairs[i][2][1])
        if rel_confs[i] > 0.1:
            ax.text(shifts[i][0], shifts[i][1], round(rel_confs[i],2))
    legend_elements = [Line2D([0], [0], marker='.', linestyle="None", label='Motor movement'),
                   Line2D([0], [0], marker='x', linestyle="None", label='Cross-corr movement'),
                   Line2D([0], [0], linestyle='-', color = 'k', label='Difference')]
    ax.set_title('Motor and cross-corr moves in x and y')
    ax.set_xlabel('Offset in x (px)')
    ax.set_ylabel('Offset in y (px)')
    ax.legend(handles=legend_elements)
    plt.gca().invert_yaxis()
    plt.show()

    fig, ax = plt.subplots()
    for i in range(len(pairs)):
        ax.scatter(shifts[i][0]-pairs[i][2][0], shifts[i][1]-pairs[i][2][1])
        ax.annotate('{0}, {1}'.format(round(pairs[i][2][0]),round(pairs[i][2][1])), (shifts[i][0]-pairs[i][2][0], shifts[i][1]-pairs[i][2][1]))
    ax.set_title('The difference between motor and cross-corr moves in x and y')
    plt.show()

def fit_spline(stage_positions, stage_to_pixels, cross_cor_pos, weights):
    stage_positions = np.asarray(stage_positions)
    cross_cor_pos = np.asarray(cross_cor_pos)
    weights = [1/i for i in weights]

    q,res,rank,s=np.linalg.lstsq(stage_positions, cross_cor_pos, rcond=None)
    fitted_pos = np.matmul(stage_positions, q)

    # for i in range(len(fitted_pos)):
    #     print('Spline fit pos is {}. Stage coords in px were {}. Cross correlation suggested {} with weight of {}'.format(np.around(fitted_pos[i]), np.around(np.matmul(stage_positions[i], stage_to_pixels)), cross_cor_pos[i], np.round(weights[i])))
    return q

def tile_images(images, q, save = False, plot = False):

    est_coords = []
    for i in range(len(images)):
        stage_coords = [images[i][1], images[i][2]]
        est_coords.append(np.matmul(stage_coords, q))

    est_coords = np.asarray(est_coords)
    est_coords[:,0] -= np.min(est_coords[:,0])
    est_coords[:,1] -= np.min(est_coords[:,1])

    est_coords = est_coords.astype(int)

    tile_shape = [np.max(est_coords[:,1]) + height, np.max(est_coords[:,0]) + width, 3] #5 down, 6 across

    canvas = np.zeros(tile_shape)

    for i in range(len(images)):
        canvas[est_coords[i][1] : est_coords[i][1]+height, est_coords[i][0] : est_coords[i][0]+width] = images[i][0]

    if save is not False:
        cv2.imwrite('{}.jpg'.format(save), canvas.astype(np.uint8))

    if plot:
        plt.imshow(canvas.astype(np.uint8))
        plt.show()

dir = r'C:\Users\jakna\Source\tiling\97688031-549e-4587-b86c-bec12f4fd2e8\SCAN_2021-12-09_15-20-48'
pixel_ratios = [[-0.006960063186987883, 0.10399761381383854], [0.2147835930610197, -0.0009687096401779707]] 

images = import_images(dir, pixel_ratios)
height = images[0][0].shape[0]
width = images[0][0].shape[1]

a = (pixel_ratios[0][1]) * (width / 832)
b = (pixel_ratios[1][1]) * (height / 624)
c = (pixel_ratios[0][0]) * (width / 832)
d = (pixel_ratios[1][0]) * (height / 624)

stage_to_pixels = np.asarray([[a, b], [c, d]])

pairs = find_overlapping_pairs(images, thresh = 0.2)
pairs, shifts, rel_confs = cross_corr_pair(pairs, images, 'cv2.TM_SQDIFF', 0.4)

# for i, pair in enumerate(pairs):
#     print(pair, shifts[i])
# plot_displacements(pairs, shifts, rel_confs)

stage_moves = []
for i in range(len(pairs)):
    stage_moves.append(pairs[i][3])
q = np.asarray(fit_spline(stage_moves, stage_to_pixels, shifts, rel_confs))

# print(np.around(q, 4))
# print(np.around(np.asarray(pixel_ratios),4))

tile_images(images, q, 'all_fit', False)
tile_images(images, pixel_ratios, 'pixel_ratio', False)
tile_images(images, stage_to_pixels, 'pixel_ratio_by_shape', False)

stage_moves_1 = []
shifts_1 = []
rel_confs_1 = []
for i, _ in enumerate(stage_moves):
    if rel_confs[i] < 0.1:
        stage_moves_1.append(stage_moves[i])
        shifts_1.append(shifts[i])
        rel_confs_1.append(rel_confs[i])
    else:
        print(rel_confs[i])
q = np.asarray(fit_spline(stage_moves_1, stage_to_pixels, shifts_1, rel_confs_1))

tile_images(images, q, 'filtered_fit', False)
